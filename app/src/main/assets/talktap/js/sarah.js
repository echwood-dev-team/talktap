
var lead = null;
var leadMesh = null;
var canvas = document.getElementById('renderCanvas');
var engine = new BABYLON.Engine(canvas, true);
engine.loadingUIBackgroundColor = "#33334c";

var createScene = function () {
var scene = new BABYLON.Scene(engine);
BABYLON.SceneLoader.ShowLoadingScreen = true;

//Adding a light
var light = new BABYLON.HemisphericLight("HemiLight", new BABYLON.Vector3(0, 1, 0), scene);
var ligh = new BABYLON.DirectionalLight("dirLight", new BABYLON.Vector3(0, -1, 0), scene);
ligh.position = new BABYLON.Vector3(0, 3, 0);

//Adding an Arc Rotate Camera,
var camera = new BABYLON.ArcRotateCamera("Camera",Math.PI / 2, 1.6, 7.6, BABYLON.Vector3.Zero(), scene);
camera.setPosition(new BABYLON.Vector3(Math.PI , 0, 0))
camera.attachControl(canvas, true);

// The first parameter can be used to specify which mesh to import. Here we import all meshes
lead = BABYLON.SceneLoader.Append("models/gltf/", "Sarah.glb", scene, function (newMeshes, particleSystems, skeletons) {
scene.activeCamera = null;
console.log(skeletons);
<!--lead.compileMaterials = true;-->
lead.compileShadowGenerators = true;
lead.animationStartMode = BABYLON.GLTFLoaderAnimationStartMode.NONE;
leadMesh = newMeshes;
<!--leadMesh.animationTimeScale = .5;-->
leadMesh.lights[0].intensity = 5;
scene.createDefaultCameraOrLight(true);
scene.activeCamera.attachControl(canvas, true);
<!--scene.stopAllAnimations()-->





//Environment

	<!--var helper = scene.createDefaultEnvironment({-->
			<!--skyboxSize: 1000,-->
			<!--groundShadowLevel: 0.6,-->
		<!--});-->



console.log(lead);

//Make her face the camera
scene.activeCamera.alpha = Math.PI/2;
scene.activeCamera.upperBetaLimit = Math.PI/2;
scene.activeCamera.lowerBetaLimit =  Math.PI/2 ;
scene.activeCamera.lowerRadiusLimit = Math.PI/2* .9;
scene.activeCamera.upperRadiusLimit = Math.PI/2* .9;
scene.activeCamera.targetScreenOffset = {x: 0, y: -0.25}

scene.activeCamera.inertialBetaOffset = 2;
console.log(scene.activeCamera);

//Animation
scene.animationGroups[0].play(false);



});


return scene;
}

var c = ["a", "b", "c", "b", "c"];

function playRest () {
    if ( )
}


function playOne () {
    scene.animationGroups[0].play(false);
    scene.animationGroups[0].onAnimationGroupEndObservable.addOnce(() => {
        scene.animationGroups[1].play(false);
    })
}




    function playAnimation(animGroups) {
        var animationGroup;
        for (var i = 0; i < animGroups.length - 1; i++) {
            animationGroup = scene.animationGroups[i]
            console.log(`Adding animation ${animationGroup}`);
            animationGroup.onAnimationGroupEndObservable.add(
                function () {
                    alert('Done with ' + animationGroup + '.\r\nAdding ' + scene.animationGroups[i+1]);
//                    console.log(`Playing Animation ${i}: ${animGroups[i+1]}`);
//                    scene.animationGroups[i+1].play()
                }
            );
        }
        scene.animationGroups[0].play();
//        var duration = 0;
//        var anim = scene.getAnimationGroupByName(animGroups[0]);
//        anim.play();
//        console.log("Playing Animation ", animGroups[0]);
//        for (var l = 1; l < animGroups.length - 1; l++) {
//          duration += (anim.to * 1000) + 300;
//          anim = scene.getAnimationGroupByName(animGroups[l]);
//          console.log(`Registering Animation ${anim}, Duration: ${duration}`);
//          setTimeout(() => {
//                console.log("Playing Animation at index ", l);
//                anim.play();
//           }, duration);
//         }
    }

    function extractAndPlayAnimations (animationGroups) {
        var largeAnimationGroup = new BABYLON.AnimationGroup("Main")
                , animationGroup;
        animationGroups.forEach(grp => {
            animationGroup = scene.getAnimationGroupByName(grp);
            targetedAnimations = animationGroup.targetedAnimations;
            targetedAnimations.forEach((a) => {
                largeAnimationGroup.addTargetedAnimation(a.animation, a.target)
            })
        })
        console.log(largeAnimationGroup)
        largeAnimationGroup.play()
    }









    var scene = createScene();
    engine.runRenderLoop(() => {
                scene.render();
            });

              window.addEventListener('resize', () => {
                engine.resize();
            });
