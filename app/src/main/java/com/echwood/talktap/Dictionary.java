package com.echwood.talktap;
/**
 * Auhor : Ukwuani Barnabas @15/30GR091
 *
 *
 * This is the Dictionary Class
 */
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class Dictionary extends AlertDialog.Builder {
    Context context;
    AlertDialog.Builder dictionary;
    ListView listView;
    ArrayAdapter dictionaryAdapter;
    String[] dict = {"1","2","3","4","5","6","7","8","9","a","b", "c", "d", "e", "f", "g", "h", "i","j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
    "u", "v", "w", "x", "y", "z"};
    String[] dict_descript = {"1","2","3","4","5","6","7","8","9","a","b", "c", "d", "e", "f", "g", "h", "i","j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
            "u", "v", "w", "x", "y", "z"};
    WebView dwebView;

    public Dictionary(Context context, WebView webView) {
        super(context);
        this.context = context;
        this.dwebView = webView;
    }
     public void builder () {
        dictionary = new AlertDialog.Builder(context, R.style.AppAlertTheme);
        View dictView = View.inflate(context, R.layout.dictionary, null);
         dictionary.setView(dictView);
         listView = dictView.findViewById(R.id.dict_list);
         AlertDialog dialog = dictionary.create();




         dictionaryAdapter = new ArrayAdapter<String>(context,R.layout.dictionary_items, dict){
             @Override
             public View getView(int position, View convertView,  ViewGroup parent) {
                 View dView = View.inflate(context, R.layout.dictionary_items, null);
                 ((TextView) dView.findViewById(R.id.dictionary_title)).setText(dict[position]);
                 ((TextView) dView.findViewById(R.id.dictionary_text)).setText(dict_descript[position]);
                 return dView;

             }
         };
         listView.setAdapter(dictionaryAdapter);
         listView.setOnItemClickListener((parent, view, position, id) -> {
             try {
                 dwebView.loadUrl("javascript:playSome('" + dict[position] + "');");
             } catch ( Exception e) {
                 Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
             }
             dialog.dismiss();
         });
         dialog.show();
     }
}
