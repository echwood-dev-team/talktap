package com.echwood.talktap;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;

public class MenuAnim {
    public static MenuAnim menuAnim;
    Context context;
    ViewGroup landerParent;
    View menuSheet;
    public boolean viewISOpen = false;


    public MenuAnim(Context context, ViewGroup landerParent, View menuSheet) {
        this.context =context;
        this.landerParent = landerParent;
        this.menuSheet = menuSheet;
    }

    public static MenuAnim getMenuAnim( Context context, ViewGroup l, View j) {
        if ( menuAnim == null) {
            menuAnim = new MenuAnim(context, l ,j);
        }
        return menuAnim;
    }
    public void addMenuView() {
        //TODO
        landerParent.addView(menuSheet);
        slideRight(menuSheet);
        viewISOpen = true;

    }

    public void removeMenuView() {
        //TODO
        landerParent.removeView(menuSheet);
        slideLeft(menuSheet);
        viewISOpen = false;
    }


    // slide the view from left itself to the current position
    public void slideRight(View view){
        TranslateAnimation animate = new TranslateAnimation(
                -view.getWidth(),                 // fromXDelta
                0,                 // toXDelta
                0,  // fromYDelta
                0);                // toYDelta
        animate.setDuration(200);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideLeft(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                -(view.getWidth()),                 // toXDelta
                0,                 // fromYDelta
                0); // toYDelta
        animate.setDuration(200);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }
}
