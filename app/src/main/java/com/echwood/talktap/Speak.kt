package com.echwood.talktap

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.support.annotation.RequiresApi
import android.webkit.WebView
import android.widget.Toast
import com.echwood.talktap.translation_engine.TranslatorFactory
import com.echwood.talktap.translation_engine.ConversionCallback

class Speak (mWebView: WebView, activity: Activity) {

    val mWebView :WebView = mWebView
    val context :Activity = activity


    @RequiresApi(Build.VERSION_CODES.M)
    fun speak( ) {
        if ( context.applicationContext.checkSelfPermission(android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ) {
            context.requestPermissions(arrayOf(android.Manifest.permission.RECORD_AUDIO),1  )
        } else {
            Toast.makeText(context, "Speak, TalkTap is listening", Toast.LENGTH_SHORT).show()

            TranslatorFactory
                    .instance
                    .with(object : ConversionCallback {
                        override fun onSuccess(result: String) {
                            mWebView.loadUrl("javascript:recurPlay('${result.toLowerCase().trim().replace("[^a-zA-Z0-9\\s]", "")}')")
                            Toast.makeText(context, "You said: $result and TalkTap will fingerspell it", Toast.LENGTH_SHORT).show()
                        }
                        override fun onCompletion() {
                        }
                        override fun onErrorOccurred(errorMessage: String) {
                            Toast.makeText(context, "TalkTap Error: $errorMessage", Toast.LENGTH_SHORT).show()
                        }
                    }).initialize("Speak Now !!", context)
        }
    }



}