/**
 * Auhor : Ukwuani Barnabas @15/30GR091
 *
 *
 * This is the Conversion Callback interface
 */

package com.echwood.talktap.translation_engine

interface ConversionCallback {

    fun onSuccess(result: String)

    fun onCompletion()

    fun onErrorOccurred(errorMessage: String)

} 