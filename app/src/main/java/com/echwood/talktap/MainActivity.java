package com.echwood.talktap;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.HashMap;

public class MainActivity extends Activity {

    public WebView mWebView;
    private RelativeLayout ownView;
    View menuSheet;
    LinearLayout linlay;
    Toolbar bToolbar;
    TextView textView;
    MenuAnim menuAnim;
    EditText textPrete;
    ImageView interPrete, speed;
    boolean viewISOpen = false;

    String[] speedTag = {"0.5","1.0","1.5", "2.0"};
    int speedNum = 2;




    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textPrete = findViewById(R.id.textprete);
        interPrete = findViewById(R.id.interprete);
        speed = findViewById(R.id.speed);

        final HashMap<String, String> hm = new HashMap<>();
        hm.put("stop", "idleAction.play()");
        hm.put("walk", "walkAction.play()");
        hm.put("run", "runAction.play()");
        //init Components
        mWebView = (WebView) findViewById(R.id.mWebView);
        mWebView.setBackgroundColor(getResources().getColor(R.color.main_col));
        ownView = findViewById(R.id.ownView);
        bToolbar = findViewById(R.id.toolbar_home);

            setActionBar(bToolbar);
            getActionBar().setDisplayShowHomeEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
            getActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            getActionBar().setDisplayHomeAsUpEnabled(true);


        //init Classes
        menuViewLoader();
        menuAnim = MenuAnim.getMenuAnim(this, ownView, menuSheet);


        bToolbar.setNavigationOnClickListener(view -> {
            if (!viewISOpen) addMenuView();
            else removeMenuView();
        });


        mWebView.loadUrl("file:///android_asset/talktap/james.html");

        // Force links and redirects to open in the WebView instead of in a browser
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageCommitVisible(WebView view, String url) {
                super.onPageCommitVisible(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Toast.makeText(MainActivity.this, "Started", Toast.LENGTH_SHORT).show();
            }
        });
        mWebView.setWebChromeClient(new WebChromeClient());


        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();

        webSettings.setAllowContentAccess(true);

        webSettings.setAllowFileAccess(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setJavaScriptEnabled(true);

        linlay = findViewById(R.id.linlay);
        findViewById(R.id.interprete).setOnClickListener(v -> {
            if (((EditText) findViewById(R.id.textprete))
                    .getText()
                    .toString().isEmpty()) {
                Toast.makeText(MainActivity.this,
                        "Recorder soon Available",
                        //TODO
                        Toast.LENGTH_SHORT)
                        .show();
            try {
                new Speak(mWebView, MainActivity.this).speak();
            } catch (Exception ex) {
                Toast.makeText(this, ex + " ", Toast.LENGTH_SHORT).show();
            }




            } else if (((EditText) findViewById(R.id.textprete))
                    .getText().toString().toLowerCase().trim().length() == 1) {
                mWebView.loadUrl("javascript:playSome('" + ((EditText) findViewById(R.id.textprete))
                        .getText().toString().toLowerCase().trim() + "');");

            }

            else {
                mWebView.loadUrl("javascript:recurPlay('" + ((EditText) findViewById(R.id.textprete))
                        .getText().toString().toLowerCase().trim().replaceAll("[^a-zA-Z0-9\\s]","")+ "');");

                Toast.makeText(MainActivity.this, ((EditText) findViewById(R.id.textprete))
                        .getText().toString().toLowerCase().trim()+ "" , Toast.LENGTH_SHORT).show();

            }


        });


        textPrete.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if ( count <= 0 && textPrete.getText().toString().isEmpty()) {
                    interPrete.setImageDrawable(getDrawable(R.drawable.ic_mic_black_24dp));
                } else {
                    interPrete.setImageDrawable(getDrawable(R.drawable.ic_send_black_24dp));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        speed.setOnClickListener( v -> {
            if (speedNum < speedTag.length) {
                mWebView.loadUrl("javascript:playSpeed('" + speedTag[speedNum] +"');");
                ((TextView)findViewById(R.id.speedText)).setText("X"+speedTag[speedNum++] );
            } else {
                speedNum = 0;
                mWebView.loadUrl("javascript:playSpeed('" + speedTag[speedNum] +"');");
                ((TextView)findViewById(R.id.speedText)).setText("X"+speedTag[speedNum++] );

            }
        });

    }
    // Prevent the back-button from closing the app
    @Override
    public void onBackPressed() {

        if(viewISOpen) {
            removeMenuView();
        }else {
            super.onBackPressed();
        }
    }

    private void menuViewLoader() {
        menuSheet = View.inflate(this, R.layout.bottomsheet, null);
        try {
            menuSheet.findViewById(R.id.dictionary).setOnClickListener(v -> {
                new Dictionary(MainActivity.this, mWebView).builder();
                removeMenuView();
            });

            menuSheet.findViewById(R.id.share).setOnClickListener(v -> {
                Intent share = new Intent(Intent.ACTION_SEND);

                removeMenuView();
            });
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    public void addMenuView() {
        ownView.addView(menuSheet);
        slideRight(menuSheet);
        viewISOpen = true;

    }

    public void removeMenuView() {
        ownView.removeView(menuSheet);
        slideLeft(menuSheet);
        viewISOpen = false;
    }


    // slide the view from left itself to the current position
    public void slideRight(View view){
        TranslateAnimation animate = new TranslateAnimation(
                -view.getWidth(),                 // fromXDelta
                0,                 // toXDelta
                0,  // fromYDelta
                0);                // toYDelta
        animate.setDuration(200);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideLeft(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                -(view.getWidth()),                 // toXDelta
                0,                 // fromYDelta
                0); // toYDelta
        animate.setDuration(200);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    }