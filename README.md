This is an AndroidN et Three.Js Application for translating audio|text to sign language.

### Getting started

Clone and import into Android Studio.

### What's Up

The breakdown beneath is a little breakdown of what's going on

1. Android **webview** runs on chromium, runs webgl (**Three.js**)

2. Animated avatar from blender exported as **.gltf** format and loaded in three.js with **gltfloader**

3. gltf stores animations in arrays and has crossover enabled for smooth animation crossing... ("from one animation to another")

4. **Kotlin|Java** communication with **Js** - which is locally served, for animation translation... animations are mapped to names 
	
	eg. [a: a, b: b...] (pseudo)
	
@author: Ukwunani Barnabas 

love from echwood 😍